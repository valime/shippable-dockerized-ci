# Shippable: Dockerized CI

### Shippable configuration example
```
language: java

jdk:
  - oraclejdk8

env:
  - firefox="46.0"

addons:
  - selenium: "2.53"

services:
  - selenium

build:
    ci:
        - xvfb-run --server-args="-ac" mvn cobertura:cobertura
    post_ci:
        - wget https://bitbucket.org/alecsandrupruna/shippable-dockerized-ci/downloads/merge_junit_results.py
        - python merge_junit_results.py target/surefire-reports/junitreports/* > shippable/testresults/results.xml
        - cp target/site/cobertura/coverage.xml shippable/codecoverage/coverage.xml

integrations:
  notifications:
    - integrationName: email
      type: email
      recipients:
        - --last_committer
      on_success: always
      on_failure: always
```
