package shippable.dockerized.ci.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.Test;
import shippable.dockerized.ci.BaseUI;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class TestSelenium extends BaseUI {
    @Test
    public void testGooglePageTitle() {
        driver.get("http://www.google.com");
        assertThat(driver.getTitle(), is("Google"));
    }

    @Test
    public void testAmazonPageTitle() {
        driver.get("http://www.amazon.com");
        assertThat(driver.getTitle(), containsString("Amazon"));
    }

    @Test
    public void testValidUserCredential() {
        driver.get("http://www.store.demoqa.com");
        driver.findElement(By.xpath(".//*[@id='account']/a")).click();
        driver.findElement(By.id("log")).sendKeys("alecspr");
        driver.findElement(By.id("pwd")).sendKeys("Irz^)C@ib4NRWggY");
        driver.findElement(By.id("login")).click();
        Wait wait = new FluentWait(driver).withTimeout(10, SECONDS).pollingEvery(1, SECONDS).ignoring(NoSuchElementException.class);
        wait.until(elementToBeClickable(By.xpath("//a[@title='Logout']")));
        assertThat(driver.findElements(By.xpath("//a[@title='Logout']")).size(), is(1));
    }

    @Test
    public void testInvalidUserCredential() {
        driver.get("http://www.store.demoqa.com");
        driver.findElement(By.xpath(".//*[@id='account']/a")).click();
        driver.findElement(By.id("log")).sendKeys("alecspr");
        driver.findElement(By.id("pwd")).sendKeys("istc");
        driver.findElement(By.id("login")).click();
        Wait wait = new FluentWait(driver).withTimeout(10, SECONDS).pollingEvery(1, SECONDS).ignoring(NoSuchElementException.class);
        wait.until(elementToBeClickable(By.xpath("//p[@class='response']")));
        assertThat(driver.findElement(By.xpath("//p[@class='response']")).getText(), is("ERROR: Invalid login credentials."));
    }
}
